const express = require('express');
const converter = require('../converter')
const app = express();
const port = 3000;

app.use(express.text())



function fullName(firstName, lastName) {
  return firstName + " " + lastName;
}

app.get("/", (req, res) => {
  res.send('Got your message');

})

app.get("/data", (req, res) => {
  res.send('Here is your data:123');

})

app.get("/name-query", (req, res) => {

  //calling the function and assigning the return to a variable
  var ff = fullName("Yoseph", "Tibebu");

  console.log(ff)
  res.send('Your name is ' + fullName(req.query.fname, req.query.lname));


})

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
});

app.get("/rgb-to-hex", (req, res) => {
  console.log('Converting RGB to HEX')
  res.send('HEX VALUE ' + converter.rgbToHex(req.query.red, req.query.green, req.query.blue));

})

app.get("/hex-to-rgb", (req, res) => {
  console.log('Converting HEX to RGB of ', req.query.hex)
  res.send('RGB ' + converter.hexToRgb(req.query.hex));
});





