import http.client

conn = http.client.HTTPConnection("localhost:3000")

headers = { "Content-Type" : "text/html; charset=utf-8" }

conn.request("GET", "http://localhost:3000/", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))



conn.request("GET", "http://localhost:3000/data", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))

conn.request("GET", "http://localhost:3000/name-query?fname=Yoseph&lname=Tibebu", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))


conn.request("GET", "http://localhost:3000/rgb-to-hex?red=34&green=67&blue=45", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))

conn.request("GET", "http://localhost:3000/hex-to-rgb?hex=0033ff", headers=headers)

res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
