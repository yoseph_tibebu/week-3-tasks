function componentToHex(c) {
    c = parseInt(c);
    var hex = c.toString(16).toUpperCase();
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

console.log(rgbToHex(0, 51, 255)) // #0033ff

function hexToRgb(hex) {
    /*console.log('The hex ', hex)*/
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    return result ? "r:" + parseInt(result[1], 16) + " "
        + "g: " + parseInt(result[2], 16) + " "
        + "b: " + parseInt(result[3], 16) : null;
}

console.log(hexToRgb("#0033ff").g); // "51";

module.exports = { rgbToHex, hexToRgb }